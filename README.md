<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 15">
<meta name=Originator content="Microsoft Word 15">
<link rel=File-List href="README_files/filelist.xml">
<link rel=Edit-Time-Data href="README_files/editdata.mso">
<style>
</style>
</head>

<body lang=EN-US style='tab-interval:.5in;word-wrap:break-word'>

<div class=WordSection1>

<div>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'>Project Flow
Guidelines<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'>Under Product<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'>Step 1: Create
a Story on the Zentao project server (note server is not accessible off campus)<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'>Step 2: Review
and Approve Story<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'>Under Project<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'>Step 1: Link Story
to the Project<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'>Step 2: Make
sure the person that should work on the story is assigned to it.<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'>Step 3: Move
store to Ready<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'>Step 4: Create
the tasks for the Story � For each task assign someone to that task. (Name for
the task should be &lt;Story&gt;_&lt;Task&gt;)<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'>Step 5: Start
the tasks <o:p></o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'>Under GitLab<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'>Step 1: Create
a branch with name of the task (Task names without spaces can be easily copied
in as the name)<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'>Step 2: In
directory with your clone of the project:<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'><span
style='mso-spacerun:yes'>������������� </span>git pull<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'><span
style='mso-spacerun:yes'>������������ </span>git checkout &lt;branchname&gt;<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'>Step 3: Make
all the changes to complete the task<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'>Step 4: Add
changes to the local repo:<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'><span
style='mso-spacerun:yes'>�������������� </span>git add *<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'><span
style='mso-spacerun:yes'>�������������� </span>git commit -m �&lt;Message
Here&gt;�<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'>Step 5: Add
changes to remote Repo (GitLab):<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'><span
style='mso-spacerun:yes'>�������������� </span>git push<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'>Step 6: Merge
changes into main<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'><span
style='mso-spacerun:yes'>������������� </span><span
style='mso-spacerun:yes'>�</span>Submit Merge Request: &lt;Your Branch&gt; into
main<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'><span
style='mso-spacerun:yes'>�������������� </span>Review request and hit �Merge�<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span style='font-family:"Aptos Serif",serif'>Note normally there
would be specific roles for this, but best to get practice on the full chain.<span
style='mso-spacerun:yes'>� </span>After that we can be more roles focused and add
that to the process.<o:p></o:p></span></p>

<p class=MsoNormal align=center style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:center'><b><i><span style='font-size:16.0pt;font-family:"Aptos Serif",serif'>Game
Dev Club Spring �24 Proposal</span></i></b></p>

<p class=MsoNormal align=center style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:center'><b><i><span style='font-size:16.0pt;font-family:"Aptos Serif",serif'>Written
by � Keegan Brenneis</span></i></b></p>

<p class=MsoNormal align=center style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:center'><b><span style='font-family:"Aptos Serif",serif'>The
following is merely a proposal for a potential plot, characters and setting of our
game for the semester. Anything may be subject to change if necessary.</span></b></p>

<p class=MsoNormal align=center style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:center'><b><span style='font-family:"Aptos Serif",serif;
color:red'>Anything marked in RED is borrowed from the original game and can be
omitted if needed.</span></b></p>

<p class=MsoNormal align=center style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
auto;text-align:center'><span style='font-size:20.0pt;font-family:"Aptos Serif",serif;
color:#5B9BD5'>Tentative name �In the Hall of the Robot King�</span></p>

<h1><span style='font-family:"Aptos Serif",serif;mso-fareast-font-family:"Times New Roman"'>Story</span><span
style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></h1>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif'>Sometime in the not-too-far-flung
future (20XX), DENSYS Power and Resources Company is the leading manufacturer
and supplier of energy and energy usage equipment in the country and
simultaneously has a world-class engineering division. This division was responsible
for creating the UNI, a revolutionary model of humanoid robot capable of
advanced movement and self-intelligence. While such a concept was not new by
this point in time, UNI was capable of much more movement and personality than
any known robot. Seeing potential in their new creation, they began to mass
employ these robots to work in their power facilities. A few years later, a new
president of the company is elected, one Shouji Furugane. The passionate and
hard-working robots leave a lasting impression on him� however, he is very
concerned about the costs of manufacturing and upkeep of so many machines.
Considering this, he elects the engineering division to make a new, economical
robot that can be manufactured cheaper and do more work than the current model.
This effort results in the DUO, a brand-new line of machines that costs
thousands less to manufacture, but at the cost of being simple machines that
exist to do work and nothing else. Delighted with the low cost of these
machines, he forces security to hijack and disengage all UNI models they can
find. You step into the role of UNI-427, nickname �Koujirou�, elected as the
overseer of the third floor of maintenance. Upon the rest of his floor being
dispatched of UNI models, they go after him and throw him into a deep dumping
pit far under the facility where his fellow dismembered and broken androids
lay. However, Koujirou was not forced to shut off like the rest of his crew
completely on accident, allowing him to survive the fall mostly intact, if a
little dinged in some spots. Upon rebooting (regaining consciousness), he
discovers what�s happened to his fellow crew and discovers that he�s been
thrown into the abandoned, underground facility, untouched for several years.
Following a brief exploration of the area, he discovers that the underground
structure is inherently maze-like, constructed as such to deter thieves.
Eventually, he discovers a massive cache of gamma energy crystals, which the
old facility uses to draw power from, and also allows the elevators on each
floor to be powered. Now it is up to Koujirou to make his way up the various
floors of the facility, carefully dropping energy crystals in these winding
structures into machines to slowly repower the old facility from underneath and
eventually face his ultimate enemy, Mr. Furugane.</span></p>

<h1><span style='font-family:"Aptos Serif",serif;mso-fareast-font-family:"Times New Roman"'>Characters</span><span
style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></h1>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b><span
style='font-family:"Aptos Serif",serif'>As of now I�m currently working on
character designs, and I will post them to the #game-dev chat as I work on
them. </span></b></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-size:18.0pt;font-family:"Aptos Serif",serif'>UNI-427</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif'>The charismatic leader of floor 3,
UNI-427, given the nickname �Koujirou� by the employees he oversees, is a robot
who has been looking after his crew and maintaining the third floor of the
facility for several years. Following the dispatching of UNI models, he is
thrown into the abandoned sector of the facility, somehow surviving, (mostly)
unscathed. He now must make his way through the abandoned facility to face off
against his new ultimate archrival. His scarf is orange to represent the floor
he works on, as every floor is color coded. Additionally, his face, which is a
big display, can show a variety of emotions, and can even be programmed to show
anything.</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><i><span
style='font-family:"Aptos Serif",serif'>�Koujirou, on duty! Nothing will get in
the way of helping my co-workers succeed!�</span></i></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-size:18.0pt;font-family:"Aptos Serif",serif'>Shouji Furugane</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif'>The new CEO and president of DENSYS
Power and Resources Company, he desires to set the company in a brand-new
direction � one that favors his ways and saves him as much money as he can. He
was responsible for assigning the engineers to create DUO, the cost-effective
robots to replace UNI.</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><i><span
style='font-family:"Aptos Serif",serif'>�It�s� unfortunate that we must abandon
these things. However, I personally believe that we need to in order to create
a more orderly and less intensive environment.�</span></i></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-size:18.0pt;font-family:"Aptos Serif",serif'>Hideyoshi Izumi</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif'>A humble maintenance worker, formerly
an engineer that helped build the UNI line, and one of the only human employees
left working at the company. Fearing that his job will soon be taken by the
rise of DUO, he aims to help Koujirou wherever he can. He meets Koujirou early
on and gives him the baton he lost that allows him to break tiles.</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><i><span
style='font-family:"Aptos Serif",serif'>�Look, bud. I know you�ve probably got
a bad perspective of humans right now, but listen to me, if you can. We�re both
in a similar spot right now, my job�s in danger too. Maybe if we work together�
we can get out of this horrible situation.�</span></i></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-size:18.0pt;font-family:"Aptos Serif",serif'>DUO</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif'>DUO is the robot that Furugane told the
engineers to create to replace the UNI. Unlike the expressive UNI, DUOs do not
have a personality or any expression aside from being normal or angry. They
will go out of their way to prevent Koujirou from getting the crystal to the
power generator. There are two variants of DUO:</span></p>

<p class=MsoListParagraph style='text-indent:-.25in'><span style='font-family:
"Aptos Serif",serif'>-</span><span style='font-size:7.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><b><span style='font-family:"Aptos Serif",serif'>DUO-X, </span></b><span
style='font-family:"Aptos Serif",serif'>a tall robot on wheels that acts as the
main threat to Koujirou�s goal. They will try to corner and crush the crystal,
as well as go after Koujirou himself to stun him.</span></p>

<p class=MsoListParagraph><i><span style='font-family:"Aptos Serif",serif'>�SHINY
GEM SPOTTED. MUST. DESTROY.�</span></i></p>

<p class=MsoListParagraph style='text-indent:-.25in'><span style='font-family:
"Aptos Serif",serif'>-</span><span style='font-size:7.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><b><span style='font-family:"Aptos Serif",serif'>DUO-Y, </span></b><span
style='font-family:"Aptos Serif",serif'>a flying robot that will appear from
either side of the screen and drop a block onto the crystal if the player sits
idle for too long. If the robot flies in, make sure to move the crystal out of
the way of the block. Also, they�re the only one of the two that made an
attempt to have a personality; unfortunately, all they can say is snarky
remarks.</span></p>

<p class=MsoListParagraph><i><span style='font-family:"Aptos Serif",serif'>�CAW!
NOT ON MY WATCH!�</span></i></p>

<h1><span style='font-family:"Aptos Serif",serif;mso-fareast-font-family:"Times New Roman"'>Gameplay</span><span
style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></h1>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif'>The game plays akin to Sega�s <i>Doki
Doki Penguin Land. </i>Koujirou enters each floor through a low power elevator,
rolling a massive gamma energy crystal. His objective is to get the energy
crystal from the entrance to the power generator at the bottom of the floor,
which will power the elevator to the next floor.</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif'>DUOs, the enemy robots, roam the
floors, posing a threat both to Koujirou and the crystal.</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif'>The key factor of gameplay is the
crystal itself. The crystal is very fragile, and will break and cause a life
loss if any of the following occurs:</span></p>

<p class=MsoListParagraph style='text-indent:-.25in'><span style='font-family:
"Aptos Serif",serif'>-</span><span style='font-size:7.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>The crystal falls below
the flashing line (~3 tiles downwards);</span></p>

<p class=MsoListParagraph style='text-indent:-.25in'><span style='font-family:
"Aptos Serif",serif'>-</span><span style='font-size:7.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>A DUO corners the crystal
or lands on it vertically;</span></p>

<p class=MsoListParagraph style='text-indent:-.25in'><span style='font-family:
"Aptos Serif",serif'>-</span><span style='font-size:7.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>The crystal is bounced on
without any space (at least one tile) to the immediate left or right of itself.</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif'>Additionally, DUOs can attack Koujirou
himself; however, doing so simply stuns him for a few seconds.</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif'>The baton is retrieved from Hideyoshi
at some point very early on. This baton can break tiles.</span></p>

<p class=MsoListParagraph style='text-indent:-.25in'><span style='font-family:
"Aptos Serif",serif;color:red'>-</span><span style='font-size:7.0pt;color:red'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif;color:red'>THIS SHOULD BE
DISCUSSED: Should it be treated like the original game where you simply look
left and right and pressing the Dig button breaks the block to the immediate
bottom left or bottom right of Koujirou, or should it be allowed to let him
break blocks immediately around him? This is important, as levels will have to
be designed around this.</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif'>Koujirou can jump roughly a tile and a
half upwards and a tile forward or backward. <span style='color:red'>The
original game has the penguin be able to jump 3 tiles, but that�s only because
he was one tile large. </span>Koujirou stands roughly two tiles tall.</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif'>A button can be held down to restart
the level, as players may get caught in a soft lock; in most cases, it will
likely happen if the player becomes too detached from the crystal with no way
to get back to it (e.g. falling too far from it).</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif'>The baton (mentioned above) cannot hurt
DUOs; however, hitting them will not penalize the player, as the player may
assume that they can hit them.</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif;color:red'>A timer is active that counts
down from 120. While nothing happens if the timer hits zero, completing the
level in that timeframe will net extra points and a time bonus on the next
level (adding the remaining seconds to the timer on the next level)</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif'>Different block types:</span></p>

<p class=MsoListParagraph style='margin-left:1.0in;text-indent:-.25in'><span
style='font-family:"Courier New"'>o</span><span style='font-size:7.0pt'>&nbsp;&nbsp;
</span><b><span style='font-family:"Aptos Serif",serif'>Wood blocks </span></b><span
style='font-family:"Aptos Serif",serif'>are the most common type. These can be
broken with the baton. </span></p>

<p class=MsoListParagraph style='margin-left:1.0in;text-indent:-.25in'><span
style='font-family:"Courier New"'>o</span><span style='font-size:7.0pt'>&nbsp;&nbsp;
</span><b><span style='font-family:"Aptos Serif",serif'>Soggy wood blocks</span></b><span
style='font-family:"Aptos Serif",serif'> will break only when the crystal is
rolled onto them. Koujirou cannot break them himself; he�s afraid of wet things
for obvious reasons.</span></p>

<p class=MsoListParagraph style='margin-left:1.0in;text-indent:-.25in'><span
style='font-family:"Courier New"'>o</span><span style='font-size:7.0pt'>&nbsp;&nbsp;
</span><b><span style='font-family:"Aptos Serif",serif'>Metal blocks </span></b><span
style='font-family:"Aptos Serif",serif'>are permanent and cannot be broken or
moved in any way.<b> </b></span></p>

<p class=MsoListParagraph style='margin-left:1.0in;text-indent:-.25in'><span
style='font-family:"Courier New"'>o</span><span style='font-size:7.0pt'>&nbsp;&nbsp;
</span><b><span style='font-family:"Aptos Serif",serif'>Timed blocks</span></b><span
style='font-family:"Aptos Serif",serif'> are the same as metal blocks, but they
disappear and re-appear in 5 second cycles.</span></p>

<p class=MsoListParagraph style='margin-left:1.0in;text-indent:-.25in'><span
style='font-family:"Courier New"'>o</span><span style='font-size:7.0pt'>&nbsp;&nbsp;
</span><b><span style='font-family:"Aptos Serif",serif'>Moving platform blocks </span></b><span
style='font-family:"Aptos Serif",serif'>are the same as metal blocks, but they
move on their own and can support the crystal and/or Koujirou.</span></p>

<p class=MsoListParagraph style='margin-left:1.0in;text-indent:-.25in'><span
style='font-family:"Courier New"'>o</span><span style='font-size:7.0pt'>&nbsp;&nbsp;
</span><b><span style='font-family:"Aptos Serif",serif'>Stones </span></b><span
style='font-family:"Aptos Serif",serif'>are� well, stones that rest on other
blocks. If the block below it is broken (or in the case of a timed block,
disappears), the stone will fall. This can be used to crush DUOs, but can also
crush the crystal if you�re not watching out.</span></p>

<p class=MsoListParagraph style='margin-left:1.0in;text-indent:-.25in'><span
style='font-family:"Courier New";color:red'>o</span><span style='font-size:
7.0pt;color:red'>&nbsp;&nbsp; </span><b><span style='font-family:"Aptos Serif",serif;
color:red'>Tubes </span></b><span style='font-family:"Aptos Serif",serif;
color:red'>can be used to move a crystal from above by hitting the tube with
the baton, as long as the crystal is sitting immediately on top of the tube.</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif'>The left-and-right-most walls cannot be
broken.</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif'>The crystal should be treated as a
square boundary, NOT as a sphere, to prevent possible bugs with manipulating
gravity and physics.</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b><span
style='font-family:"Aptos Serif",serif'>&nbsp;</span></b></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif;color:red'>&nbsp;</span></p>

<h1><span style='font-family:"Aptos Serif",serif;mso-fareast-font-family:"Times New Roman"'>Controls</span><span
style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></h1>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='mso-no-proof:yes'><img width=624 height=344 id="_x0000_i1025"
src="README.md_files/image001.png"></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif'>The following diagram depicts the ideal
default controls on a controller. Keyboard binds can be discussed on a later
date.</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif'>A PlayStation digital controller is
depicted but can be substituted for any other controller with a similar layout.
Ideally, the layout can be changed in-game, but this will depend on the
complexity of doing so in Godot.</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif'>Koujirou moves left and right with the
D-Pad, jumps with Cross or Triangle, and can dig with Circle and Square. When
he touches the crystal and keeps going in the same direction, he will
automatically push the crystal in that direction.</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif'>Make note of the HOLD on L1; making it
so that the button must be held for ~2-3 seconds prevents accidental do-overs.
A visual prompt saying that the level will restart must show up as soon as the
button is pressed.</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif'>Depending on how we go about
implementing controls, these are the two valid control styles for �digging�:</span></p>

<p class=MsoListParagraph style='text-indent:-.25in'><span style='font-family:
"Aptos Serif",serif'>-</span><span style='font-size:7.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>MIMICKING THE ORIGINAL:
Koujirou will destroy the block immediately to the bottom left or right of him
when Dig is pressed, if possible, dependent on the direction he is facing. (If
he is facing the left, he will break the block bottom left of him, vice versa)</span></p>

<p class=MsoListParagraph style='text-indent:-.25in'><span style='font-family:
"Aptos Serif",serif'>-</span><span style='font-size:7.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>MODERNIZING THE CONTROLS:
When Circle or Square is held, a square reticle shows up around the player. Use
the D-Pad to move the reticle around (limited to exclusively the tiles
immediately surrounding him), and when the button is lifted, Koujirou will break
the block with the baton if it can be broken.</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif'>When the game is paused with START,
down and up can be used to pan across the entire level. Pressing START again
unpauses the game.</span></p>

<h1><span style='font-family:"Aptos Serif",serif;mso-fareast-font-family:"Times New Roman"'>&nbsp;</span><span
style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></h1>

<h1><span style='font-family:"Aptos Serif",serif;mso-fareast-font-family:"Times New Roman"'>Explanations
of Naming Schemes</span><span style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></h1>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b><span
style='font-family:"Aptos Serif",serif'>DENSYS Power and Resources Company</span></b></p>

<p class=MsoListParagraph style='text-indent:-.25in'><span style='font-family:
"Aptos Serif",serif'>-</span><span style='font-size:7.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>Densys is a shortening of
�Denshi Systems�, of which <i>denshi </i>simply means �electricity�.</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b><span
style='font-family:"Aptos Serif",serif'>UNI-427 �Koujirou�</span></b></p>

<p class=MsoListParagraph style='text-indent:-.25in'><span style='font-family:
"Aptos Serif",serif'>-</span><span style='font-size:7.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>UNI is an effective
shortening of �university�, alongside doubling as a way to say �one�.</span></p>

<p class=MsoListParagraph style='text-indent:-.25in'><span style='font-family:
"Aptos Serif",serif'>-</span><span style='font-size:7.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>The number 42 can be used
as Japanese number play (goroawase) to mean �shi-pu�.</span></p>

<p class=MsoListParagraph style='text-indent:-.25in'><span style='font-family:
"Aptos Serif",serif'>-</span><span style='font-size:7.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>427 is the number of the
lone employee from <i>The Stanley Parable, </i>implied here as Koujirou is the
last of his line that�s still operational.</span></p>

<p class=MsoListParagraph style='text-indent:-.25in'><span style='font-family:
"Aptos Serif",serif'>-</span><span style='font-size:7.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>Koujirou is a Japanese
surname with two meanings:</span></p>

<p class=MsoListParagraph style='margin-left:1.0in;text-indent:-.25in'><span
style='font-family:"Courier New"'>o</span><span style='font-size:7.0pt'>&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>Kou means either �god or
deity� or �heart, soul and excellence�</span></p>

<p class=MsoListParagraph style='margin-left:1.0in;text-indent:-.25in'><span
style='font-family:"Courier New"'>o</span><span style='font-size:7.0pt'>&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>Jiro is �a castle or
fortress�.</span></p>

<p class=MsoListParagraph style='margin-left:1.0in;text-indent:-.25in'><span
style='font-family:"Courier New"'>o</span><span style='font-size:7.0pt'>&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>� it also stemmed from a
misunderstanding on my part that Koujirou is the Japanese word for �orange�,
his scarf color and the complimentary color of blue, but it�s a cool surname
regardless.</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b><span
style='font-family:"Aptos Serif",serif'>DUO</span></b></p>

<p class=MsoListParagraph style='text-indent:-.25in'><span style='font-family:
"Aptos Serif",serif'>-</span><span style='font-size:7.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>The UNI is �one�, so DUO
is obviously �two� as there�s two models of it.</span></p>

<p class=MsoListParagraph style='text-indent:-.25in'><span style='font-family:
"Aptos Serif",serif'>-</span><span style='font-size:7.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>DUO is the �second
generation� after UNI despite being an obvious downgrade.</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b><span
style='font-family:"Aptos Serif",serif'>Shouji Furugane</span></b></p>

<p class=MsoListParagraph style='text-indent:-.25in'><span style='font-family:
"Aptos Serif",serif'>-</span><span style='font-size:7.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>Shouji means �commander or
administrator�, and Furugane means both �older [person] or ancient� or �money,
or gold�.</span></p>

<p class=MsoListParagraph style='text-indent:-.25in'><span style='font-family:
"Aptos Serif",serif'>-</span><span style='font-size:7.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>Originally was going to be
an Englishman named Sawyer Barton, but I scrapped the name for it sounding a
little strange. </span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b><span
style='font-family:"Aptos Serif",serif'>Hideyoshi Izumi</span></b></p>

<p class=MsoListParagraph style='text-indent:-.25in'><span style='font-family:
"Aptos Serif",serif'>-</span><span style='font-size:7.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>Hideyoshi means �of good
luck� or �outstanding�, both to denote the luck he has in meeting Koujirou and
also to reference that he was one of the engineers that created the UNI line.</span></p>

<p class=MsoListParagraph style='text-indent:-.25in'><span style='font-family:
"Aptos Serif",serif'>-</span><span style='font-size:7.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>Izumi means �spring�; this
doesn�t contribute much to any meaning behind the character, but I felt it fit
well with the first name.</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b><span
style='font-family:"Aptos Serif",serif'>In the Hall of the Robot King</span></b></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif'>This name came to me when researching
names for the other characters. Upon seeing that Koujirou�s name has some
semblance to �god or deity�, I wanted to see if I could think of something to
go alongside it. The name, obviously, stems from the orchestral piece �In the
Hall of the Mountain King�, an iconic piece whose initially slow but
progressively faster tempo has made it very popular for appropriate situations
in film and television. Additionally, the �Robot King� could theoretically
refer to Furugane, the CEO of the company, as technically that�s what he is.
Also, �hall� is a good description of the labyrinths our hero has to solve.</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b><span
style='font-family:"Aptos Serif",serif'>Why all the Japanese names? You don�t
even know the language!</span></b></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif'>Well, a lot of my influence on
literally everything I do when it comes to new ideas for games or stories has a
lot of Japanese influence. A majority of games I played as a kid were developed
by Japanese companies (and I still have a passion for a lot of them, especially
JRPGs) and I also watched a lot of anime growing up (4kids, anyone?). I also
have a wider understanding of how Japanese naming schemes work, and therefore I
tend to use them more often than English ones. Plus, I�m kind of bad at
thinking of unusual English names that aren�t super weird sounding.</span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
style='font-family:"Aptos Serif",serif'>If you�d like to suggest different
names for the human characters or even the company, go for it; Koujirou,
however, is a name that I think fits the protagonist super well so I�d like to
stick to that.</span></p>

<h1><span style='font-family:"Aptos Serif",serif;mso-fareast-font-family:"Times New Roman"'>A
Few Last Things</span><span style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></h1>

<p class=MsoListParagraph style='text-indent:-.25in'><span style='font-family:
"Aptos Serif",serif'>-</span><span style='font-size:7.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>I plan on doing some voice
acting for the characters. For example, I plan on voicing Koujirou myself,
using Audacity to modify my voice to make it sound more electronic. If anyone
is willing to provide extra voice clips once I figure out what I need, let me
know, because I want a range of different voices for the 5 total characters
that need clips. A majority of it will be noises for actions like jumping</span></p>

<p class=MsoListParagraph style='text-indent:-.25in'><span style='font-family:
"Aptos Serif",serif'>-</span><span style='font-size:7.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><i><span style='font-family:"Aptos Serif",serif'>Only if I have the time
to, </span></i><span style='font-family:"Aptos Serif",serif'>I might consider
making an animated opening of some sort, since Godot natively supports video
playback using the rather unusual .ogv format. This is mainly to help me get to
learn Blender more, as I do 3D animation and modelling as a side hobby and want
to get better at it. Will likely be either in 720p or 1080p, 15 frames per
second. Actually rendering the entire thing though� that�ll be a bridge I can
cross when I get there.</span></p>

<p class=MsoListParagraph style='text-indent:-.25in'><span style='font-family:
"Aptos Serif",serif'>-</span><span style='font-size:7.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>The visual style of the
game is yet to be determined; I wanted to attempt to do some form of
pre-rendered spritework, but I�d have to look into it more.</span></p>

<p class=MsoListParagraph style='text-indent:-.25in'><span style='font-family:
"Aptos Serif",serif'>-</span><span style='font-size:7.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>Sound effects will likely
be acquired from Freesound, which appears to have already been used a few times
by the club in the past. Music is another story, and if it comes down to it, I
might consider asking some of my Discord friends if they�re capable of making
decent music.</span></p>

<p class=MsoListParagraph style='text-indent:-.25in'><span style='font-family:
"Aptos Serif",serif'>-</span><span style='font-size:7.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>In the event that we�re
not able to finish it by the deadline and/or if we decide to move on to
something else, I will likely attempt to continue the project myself, because
I�m super proud of how well creating this little world went.</span></p>

<p class=MsoListParagraph style='margin-left:1.0in;text-indent:-.25in'><span
style='font-family:"Courier New"'>o</span><span style='font-size:7.0pt'>&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>My main reason for saying
this is because this proposal, from what I can tell, seems to be the largest
project that the club has ever done, due to having an extensive story and
characters. I highly suspect that we won�t get it done in a single semester, but
I would be delighted to be proven wrong. Not to say anyone on the team is bad
at programming, because everyone seems <i>very</i> capable - rather it�s that
getting the entire thing done in a mere few months is very hard to do.</span></p>

<p class=MsoListParagraph style='text-indent:-.25in'><span style='font-family:
"Aptos Serif",serif'>-</span><span style='font-size:7.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>A notice such as the
following might be required in case I missed something regarding names or etc:</span></p>

<p class=MsoListParagraph style='margin-left:1.0in;text-indent:-.25in'><span
style='font-family:"Courier New"'>o</span><span style='font-size:7.0pt'>&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>This title is a work of
fiction. Any depictions of real-life people, places, companies or situations
are purely coincidental.</span></p>

<p class=MsoListParagraph style='text-indent:-.25in'><span style='font-family:
"Aptos Serif",serif'>-</span><span style='font-size:7.0pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span><span style='font-family:"Aptos Serif",serif'>If there�s ANYTHING you�d
like to contribute, absolutely throw suggestions at me! I�d be glad to listen.</span></p>

</div>

</div>

</body>

</html>
