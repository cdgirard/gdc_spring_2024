extends CharacterBody2D
@export var NoClipSpeed: float
@export var MoveSpeed : float
@export var JumpSpeed : float
@export var Gravity : float
@export var JumpsquatFrames : int
@export var PushSlowdown : float


var animations : AnimatedSprite2D
var anims_scale : float

var direction_facing : float
var in_jumpsquat : bool

var raycast

var dev_noclip : bool

# Called when the node enters the scene tree for the first time.
func _ready():
	raycast = $AnimatedSprite2D/RayCast2D
	raycast.force_raycast_update()
	animations = $AnimatedSprite2D
	direction_facing = 1
	anims_scale = animations.scale.x
	
	dev_noclip = false
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func process_movement(dir_x,delta):
	if dir_x != 0:
		direction_facing = dir_x
	animations.scale.x = anims_scale * direction_facing
	
	if (!dev_noclip):
		# Set x velocity
		velocity.x = dir_x * MoveSpeed
		
		# Handle pushable objects
		var obj = raycast.get_collider()
		var should_push_object = (obj and abs(velocity.x) > 20 and obj.is_in_group("pushable"))
		if should_push_object:
			obj.velocity.x = self.velocity.x
			
		# Play Animations
		if is_on_floor():
			if in_jumpsquat:
				if animations.animation == "jump" and animations.frame == JumpsquatFrames:
					velocity += Vector2(0, -JumpSpeed)
			elif should_push_object:
				animations.play("pushing")
			else:
				if dir_x == 0:
					animations.play("idle")
				else:
					animations.play("running")
		else:
			in_jumpsquat = false
		
		# Handle CharacterBody movement
		if not is_on_floor():
			velocity.y += Gravity
		motion_mode = CharacterBody2D.MOTION_MODE_GROUNDED
	else:
		var up_amount = Input.get_action_strength("input-up")
		var down_amount = Input.get_action_strength("input-down")
		var dir_y = down_amount - up_amount
		velocity.y = dir_y  * NoClipSpeed
		velocity.x = dir_x * NoClipSpeed
		motion_mode = CharacterBody2D.MOTION_MODE_FLOATING
	
	
func _physics_process(delta):
	# Take Input & Move
	var left_amount = Input.get_action_strength("input-left")
	var right_amount = Input.get_action_strength("input-right")
	var dir_x = right_amount - left_amount
	process_movement(dir_x,delta)
	
	move_and_slide()
	
	
	

func _input(event):
	if event is InputEventKey:
		if event.is_action_pressed("input-jump") and is_on_floor():
			in_jumpsquat = true
			animations.play("jump")
		
		if event.is_action_pressed("reset"):
			get_tree().reload_current_scene()
		
		if event.is_action_pressed("noclip"):
			dev_noclip = !dev_noclip
			
	

