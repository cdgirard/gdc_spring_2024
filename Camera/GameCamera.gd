extends Camera2D

@export var entity_to_follow : Node2D
@export var lock_x : bool
@export var lock_y : bool

# Called when the node enters the scene tree for the first time.
func _ready():
	position.x = entity_to_follow.position.x
	position.y = entity_to_follow.position.y
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if !lock_x:
		position.x = entity_to_follow.position.x
	if !lock_y:
		position.y = entity_to_follow.position.y
