extends CharacterBody2D

@onready var Sprite = $Sprite2D
@export var Gravity : float
@export var RollAnimationSpeed : float

var drag : float = 0.5

func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y += Gravity
	
	# apply drag 
	if abs(velocity.x) > 0:
		velocity.x *= drag
	
	Sprite.rotation += delta * velocity.x * 0.05
	

	move_and_slide()
