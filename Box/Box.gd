extends CharacterBody2D

@export var Gravity : float

var drag = 0.5

func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y += Gravity
	
	# apply drag 
	if abs(velocity.x) > 0:
		velocity.x *= drag
	
	move_and_slide()
