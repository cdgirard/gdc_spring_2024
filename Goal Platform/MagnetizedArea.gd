extends Area2D

@export var strength : float

var attracting : CharacterBody2D

# Called when the node enters the scene tree for the first time.
func _ready():
	attracting = null
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if attracting != null:
		var difference = self.global_position - attracting.global_position
		attracting.velocity += difference * strength
	pass

func _on_body_entered(body):
	if body.get_name() == "Crystal":
		print("Goal!")
		attracting = body

func _on_body_exited(body):
	if body.get_name() == "Crystal":
		attracting = null
